# ansible-hadoopdev

This is a ansible-playbook for creating develop version of Hadoop(Apache release).
Currently, this scirpt assumes to work on AWS's Ubuntu.

## How to run scripts

```
# setup master
$ vim setup_master.yml
$ ansible-playbook -i hosts setup_master
```

```
# setup slave
$ vim setup_slave.yml
$ ansible-playbook -i hosts setup_slave
```

